const express = require('express');
const router = express.Router();

/* GET users listing. */
router.get('/login', (req, res) => {
    res.render('login', { title: 'Avaamo LiveAgent' });
});

// router.get('/admins', (req, res) => {
//     res.render('users', { title: 'Avaamo LiveAgent' });
// }); //put styles and check other logics

router.get('/agents', (req, res) => {
    res.render('agents', { title: 'Avaamo LiveAgent' });
}); // need to integrate the accountId API and put styles
 
router.get('/conversations', (req, res) => {
    res.render('convo', { title: 'Avaamo LiveAgent' });
});

// router.get('/addAgent', (req, res) => {
//     res.render('addAgent', { title: 'Avaamo LiveAgent' });
// }); // no need

module.exports = router;
