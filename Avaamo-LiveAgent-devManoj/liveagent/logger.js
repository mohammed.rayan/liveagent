const winston = require('winston');
const moment = require('moment-timezone');

// Get timestamp in specified time zone
const timestamp = winston.format((info, opts) => {
    if (opts.tz)
        info.timestamp = moment().tz(opts.tz).format();
    return info;
});
// Setting timestamp for India
const logFormat = winston.format.combine(
    timestamp({ tz: 'Asia/Calcutta' }),
    winston.format.json()
);

/**
 * 
 * @param {string} service  Name of the service which is writing the log
 */
const logger = (service) => {
    return winston.createLogger({
        format: logFormat,
        defaultMeta: { service: service },
        transports: [
            new winston.transports.File({ filename: './logs/app.log' }),
            new winston.transports.Console()
        ],
    });
};

module.exports = logger;