const express = require('express');
const router = express.Router();
const _ = require('lodash');
const User = require('../lib/models/user');
const Account = require('../lib/models/account');
const AccountRole = require('../lib/models/accountRole');
const UserAccount = require('../lib/models/userAccount');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const { result } = require('lodash');

router.get('/:id/users', async (req, res) => {
    // const token = req.header('Access-Token');
    const accountid = req.params.id;
    const usersAccount = await UserAccount.findByAccountId(accountid);  
    let finalResponse = [];
    for(let i=0;i<usersAccount.length;i++) {
        console.log(usersAccount[i].user)
        let user = await User.findById(usersAccount[i].user);
        let roleID = [];
        for(let j=0;j<usersAccount[i].roles.length;j++) {
            roleID.push(await AccountRole.findOne(usersAccount[i].roles[j]));
        }
        console.log("roleID", roleID);
        finalResponse.push({
            _id: usersAccount[i]._id,
            account: usersAccount[i].account,
            user: user,
            roles: roleID
        });
    }

    // TODO: Optimize the query
    return res.json(finalResponse).status(200);
});

router.post('/create', async (req, res) => {
    const token = req.header('Access-Token');
    const name = req.body.name;

    const u = await User.findByToken(token);
    // If not found
    if (_.isNull(u))
        return res.sendStatus(401);


    // Create the account
    const account = new Account();
    account.name = name;
    account.createdAt = new Date();
    account.createdBy = u;
    await account.create();

    // Create the account roles master
    const accountRole = new AccountRole();
    accountRole.account = account;
    accountRole.roles = 'Administrator';
    accountRole.create();

    // Create the user account mapping
    const userAccount = new UserAccount();
    userAccount.account = account;
    userAccount.user = u;
    userAccount.roles = [accountRole];
    await userAccount.create();

    return res.json(account).status(201);
});

router.post('/add_user', async (req, res) => {
    const token = req.header('Access-Token');
    const accountName = req.body.account;
    const role = req.body.role;

    const user = await User.findByToken(token);
    // If not found
    if (_.isNull(user))
        return res.sendStatus(401);

    const account = await Account.findByName(accountName);
    if (_.isNull(account))
        return res.sendStatus(401);

    const userAccount = new UserAccount();
    userAccount.account = account;
    userAccount.user = user;
    userAccount.roles = [role];
    await userAccount.create();
});

router.put('/:id/add_roles', async (req,res) => {
    const token = req.header('Access-Token');

    const userId = req.body.userId;
    const roles = req.body.role;

    const user = await User.findByToken(token);
    // If not found
    if (_.isNull(user))
        return res.sendStatus(401);

    const userAccount = await UserAccount.findByUserId(userId);

    if (_.isNull(userAccount))
        return res.sendStatus(401);

    let account = await UserAccount.update({ user: userId}, { $set: {
        roles: roles
    }}).exec();

    return res.json(account).status(201);
});

module.exports = router;
