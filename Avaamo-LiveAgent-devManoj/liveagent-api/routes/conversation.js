const express = require('express');
const router = express.Router();
const _ = require('lodash');
const User = require('../lib/models/user');
const Account = require('../lib/models/account');
const AccountRole = require('../lib/models/accountRole');
const UserAccount = require('../lib/models/userAccount');
const Conversation = require('../lib/models/conversation');
const Message = require('../lib/models/message');
const avaamo = require('../lib/avaamo/Avaamo');

const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

router.get('/:id/list', async (req, res) => {
    let accountId = req.params.id;

    // https://c7.avaamo.com/live_agent/custom/conversations/requests.json?access_token=c9fc2b2adf634acc9839091814dc84df
    let conversationList = await Conversation.list(accountId);
    return res.json(conversationList).status(200);
});

router.get('/list/:instance/:token', async (req, res) => {
    let accountId = req.params.instance;
    let token = req.params.token;

    // https://c7.avaamo.com/live_agent/custom/conversations/requests.json?access_token=c9fc2b2adf634acc9839091814dc84df
    let conversationList = await Conversation.list(accountId);
    return res.json(conversationList).status(200);
});

router.post('/:conversationId/acceptChat', async (req, res) => {
    let convoId = req.params.conversationId;
    const token = req.header('Access-Token');
    const user = await User.findByToken(token);

    if (_.isNull(user))
        return res.sendStatus(401);

    //update conversation
    await Conversation.updateOne({ _id: convoId }, {
        $set: {
            accepted: true,
            acceptedBy: user._id
        }
    }).exec();

    global.io.emit('chat_request_accept', {
        convo_id: convoId
    });

    let conversationDetails = await Conversation.findByAvaamoChatId(convoId);
    await avaamo.acceptChat(conversationDetails.conversation_uuid, conversationDetails.liveagentToken);
    // console.log("RESPONSE", response);
    res.json({ message: "Request Accepted" }).status(200);
});

router.post('/:conversationId/postMessage', async (req, res) => {
    let convoId = req.params.conversationId;
    const token = req.header('Access-Token');

    const user = await User.findByToken(token);
    let message = req.body.message;

    let messageDetails = {
        text: message,
        from: 'Agent'
    }

    if (_.isNull(user))
        return res.sendStatus(401);

    let conversationDetails = await Conversation.findByAvaamoChatId(convoId);
    await Message.create(convoId, messageDetails);

    let response = avaamo.sendMessage(conversationDetails.conversation_uuid, message, conversationDetails.liveagentToken);
    res.json(response).status(200);
});

router.put('/:conversationId/endChat', async (req, res) => {
    let convoId = req.params.conversationId;
    const token = req.header('Access-Token');
    const user = await User.findByToken(token);

    if (_.isNull(user))
        return res.sendStatus(401);

    await Conversation.updateOne({ _id: convoId }, {
        $set: {
            closed: true,
            closedBy: user._id
        }
    }).exec();

    let conversationDetails = await Conversation.findByAvaamoChatId(convoId);

    await avaamo.disconnect(conversationDetails.conversation_uuid, conversationDetails.liveagentToken);
    res.status(200).json({ message: "Conversation Disconnected" });
});

module.exports = router;