const express = require('express');
const router = express.Router();
const avaamo = require('../lib/avaamo/Avaamo');
const { find } = require('../lib/models/conversation');
const Conversation = require('../lib/models/conversation');
const Message = require('../lib/models/message');

router.post('/', async function (req, res, next) {
    var event = req.body.event;
    var cid = event.conversation.uuid;

    var user;
    var config = req.query;
    console.log(config);
    var io = req.app.get('socketio');
    // Events from Avaamo
    switch (event.event_type) {
        case 'new_chat_request':
            console.log("NEW CHAT")
            let convo = new Conversation();
            convo.account = config.accountId;
            convo.conversation_uuid = cid;
            convo.liveagentToken = config.liveagentToken;
            convo.guest = JSON.stringify(event.user);
            let c = await convo.save();
            console.log(c);
            global.io.emit('chat_request', { account: config.accountId, conversation_uuid: cid, liveagentToken: config.liveagentToken, conversation_id: c._id, 
                first_name: event.user.first_name, last_name: event.user.last_name })
            //socket => emit -> new message (backend)
            //socket => on -> new message (frontend) listen
            //io.sockets.emit('chat_request', { account: config.accountId, conversation_uuid: cid, liveagentToken: config.liveagentToken });
            break;

        case 'chat_request_accepted':
            // 
            break;

        case 'user_message':
            // Get the user and message
            let message = event.message.content;
            let messageDetails = {
                text: message,
                from: 'Guest'
            }
            console.log("cid=========++>>>>>>>>>", cid);
            let convoId = await Conversation.findByUUID(cid);
            console.log("convoId=========++>>>>>>>>>", convoId);
            
            let m = await Message.create(convoId._id, messageDetails);

            global.io.emit('message', {
                account: config.accountId,
                conversation_uuid: cid,
                liveagentToken: config.liveagentToken,
                message: message
            });

            // socket -> emit -> message
            // socket -> on -> (frontend)
            break;

        case 'chat_terminated':
            let convoDetails = await Conversation.findByUUID(cid);
            console.log("c===>", convoDetails);
            await Conversation.updateOne({ _id: convoDetails._id }, {
                $set: {
                    closed: true
                }
            }).exec();
            // socket -> emit -> disconnected
            break;
    }
    res.send().status(200);

});

module.exports = router;