const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ConversationSchema = new Schema({
    account: { type: Schema.Types.ObjectId, ref: 'Account' },
    liveagentToken: String,
    accepted: Boolean,
    closed: Boolean,
    acceptedBy: { type: Schema.Types.ObjectId, ref: 'User' },
    closedBy: { type: Schema.Types.ObjectId, ref: 'User' },
    conversation_uuid: String,
    guest: String
});

ConversationSchema.methods = {
    // accept: function (user) {
    //     this.accepted = true;
    //     this.acceptedBy = user;
    //     this.save();
    // },
    // close: function (user) {
    //     this.closed = true;
    //     this.closedBy = user;
    //     this.save();
    // },
    create: function () {
        const err = this.validateSync();
        if (err && err.toString())
            throw new Error(err.toString());
        this.save();
    }
};

ConversationSchema.statics = {
    findByAvaamoChatId: function (_id) {
        return this.findOne({ _id })
            .exec();
    },
    list: function (account) {
        return this.find({ account: account })
            .exec();
    },
    findByUUID: function(uuid) {
        return this.findOne({ conversation_uuid: uuid}).exec();
    }
};

module.exports = mongoose.model('Conversation', ConversationSchema);