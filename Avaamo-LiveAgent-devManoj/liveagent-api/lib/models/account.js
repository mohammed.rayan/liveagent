const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var AccountSchema = new Schema({
    name: String,
    createdAt: Date,
    createdBy: { type: Schema.Types.ObjectId, ref: 'User' }
});

AccountSchema.path('name').required(true, 'Name cannot be blank');

AccountSchema.methods = {
    create: function () {
        const err = this.validateSync();
        if (err && err.toString())
            throw new Error(err.toString());

        this.save();
    }
};

AccountSchema.statics = {
    find: function (_id) {
        return this.findOne({ _id })
            .exec();
    },
    findByName: function (name) {
        return this.findOne({ name: name })
            .exec();
    }
};

module.exports = mongoose.model('Account', AccountSchema);