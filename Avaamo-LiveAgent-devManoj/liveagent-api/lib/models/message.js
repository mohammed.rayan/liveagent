const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var MessageSchema = new Schema({
    conversation: { type: Schema.Types.ObjectId, ref: 'Conversation' },
    createdAt: { type: Date, default: Date.now },
    message: [{
        text: String,
        from: String
    }]
});

// MessageSchema.methods = {
//     create: function () {
//         const err = this.validateSync();
//         if (err && err.toString())
//             throw new Error(err.toString());
//         this.save();
//     }
// };

MessageSchema.statics = {
    create: function (conversationId, message) {
        return this.update({ conversation: conversationId },
            {
                "$addToSet": { "message" : message },
                "$set": {
                    "conversation": conversationId,
                }
            }, { upsert: true })
            .exec();
    }
};

module.exports = mongoose.model('Message', MessageSchema);