const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

var AccountRoleSchema = new Schema({
    account: { type: Schema.Types.ObjectId, ref: 'Account' },
    roles: { type: String }
});

AccountRoleSchema.methods = {
    create: function () {
        const err = this.validateSync();
        if (err && err.toString())
            throw new Error(err.toString());

        this.save();
    }
};

AccountRoleSchema.statics = {
    findByAccountId: function (id) {
        return this.find({ account: id }).exec();
    },
    findByRoleId: function (id) {
        return this.findOne({ _id: id }).exec();
    }
};

module.exports = mongoose.model('AccountRole', AccountRoleSchema);