const { result } = require('lodash');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserAccountSchema = new Schema({
    account: { type: Schema.Types.ObjectId, ref: 'Account' },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    roles: [{ type: Schema.Types.ObjectId, ref: 'AccountRole' }],
});

UserAccountSchema.methods = {
    create: function () {
        const err = this.validateSync();
        if (err && err.toString())
            throw new Error(err.toString());

        this.save();
    },
    addUser: function (user) {
        this.users.push(user);
        this.save();
    }
};

UserAccountSchema.statics = {
    findByAccountId: function (_id) {
        return this.find({ account: _id }).exec();
    },
    findByUserId: function (_id) {
        return this.findOne({ user: _id })
            .exec();
    }
};

module.exports = mongoose.model('UserAccount', UserAccountSchema);